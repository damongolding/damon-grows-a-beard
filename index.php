<?php
include("/home3/damongo1/public_html/_sites/password_protect.php");
require "lessphp/lessc.inc.php";

$less = new lessc;
$less->setFormatter("compressed");
$less->checkedCompile("less/style.less", "css/style.css");


//path to directory to scan
$directory = "img/uploads/";

//get all image files with a .jpg extension.
$images = glob($directory . "*.jpg");
usort($images, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));

?><!DOCTYPE html>
<html lang="en">
	<head>
		<title>Damon grows a beard</title>
		<meta charset="utf-8" />
		<meta name="description" content="Damon Golding grows a beard for charity">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes" />
		
		<link rel="image_src" href="facebook-icon.png" title="facebook-icon"/>
		<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />
		
		<link rel="alternate" type="application/rss+xml" href="rss/index.xml" title="Damon grows a beard" />

		
		<link rel="stylesheet" type="text/css" href="css/foundation.css" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		
		<!--[if lte IE 8]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script src="js/css3-mediaqueries.js"></script>
		<![endif]-->
		
		<script type="text/javascript">

			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-35849230-1']);
			_gaq.push(['_trackPageview']);
			
			(function() {
			  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
			
		</script>
		
	</head>
	<body class="no-js scrollblock">
	<div id="top" class="hidden"></div>
<!-- !nav -->

	<nav class="row">
		<div class="six columns centered">
			<div class="four columns why">
				<a href="http://damongrowsabeard.tumblr.com">
					blog?
				</a>
			</div>
			
			<div class="four columns d_gallery">
				<a href="#gallery">
					gallery
				</a>
			</div>
			
			<div class="four columns donate">
				<a href="https://www.movember.com/uk/donate/payment/member_id/3138337/">
					donate
				</a>
			</div>
		</div>
	</nav>
	
<!-- !header -->

	<header>
		<div class="row ">
			<div class="nine columns centered"></div>
		</div>
	</header>
	
<!-- end header -->

<!-- !container/content -->

	<section  class="container">
		<section class="row stache">
			<div class="four column centered">
				<img src="img/stache.png" data-density1="img/stache.png" data-density1.5="img/stache@2x.png" data-density2="img/stache@2x.png" alt="stache" />
			</div>
		</section>
		
<!-- !main content -->
		
		<section class="row content ">
			<div class="six columns offset-by-one">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo beard. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			<div class="four columns">
				<a class="twitter-timeline" href="https://twitter.com/Damons_Beard" data-widget-id="258933589461311488">Tweets by @Damons_Beard</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

			</div>
		</section>
		
<!-- !gallery -->

		<section id="gallery" class="gallery">
			
			<div class="row header">
				<div class="four columns centered">
					<img src="img/gallery_header.png" data-density1="img/gallery_header.png" data-density1.5="img/gallery_header@2x.png" data-density2="img/gallery_header@2x.png" alt="Daily beard gallery" />
				</div>
			</div>
			
			<div  class="row">
			<?php
				//print each file name
				foreach($images as $image)
				{
				echo "<div class='four columns centered'>
							<div class='ten columns centered'>
								<img src='img/string.png' alt='frame string' />
							</div>
						<img class='frame' src='" . $image ."' alt='" . basename($image) ."' />
						<div class='day'>" . basename($image) ."</div>
					</div>";
				}
			?>
				
			</div>
			</section>
		</section>	
<!-- !footer -->
		<footer>
			<div class="row">
				<div id="footer_top" class="three columns offset-by-one">
					<a href="#top" >
						top
					</a>
				</div>
				<div class="four columns">
					<a href="https://www.movember.com/uk/donate/payment/member_id/3138337/">
						Donate
					</a>
					</div>
				<div id="font" class="three columns font end">Dyslexic?</div>
				<div class="one columns">
					<ul>
						<li class="subscribe">
							<a type="application/rss+xml" href="/rss/rss.rss">RSS</a>
						</li>
						<li class="rss">
							<img src="img/rss.png" alt="rss icon"/>
						</li>
					</ul>
				</div>
			</div>
		</footer>		
		
		
		<div id="thatswhy" class="reveal-modal">
			<h2>Because beards are awesome, thats why.</h2>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		
		<div id="oldbrowser" class="reveal-modal">
		<h2>Did you know that your browser is out of date?</h2>
		
		<p>To get better browsing experience and enhanced security, try one of the free, modern web browsers or upgrade to the newest version of Internet Explorer.</p>
		
		<p>Some popular web browsers - just click on the icon to get to the download page!</p>
		
		<div style="text-align: center">
			<a href="http://www.mozilla.com/firefox/" title="Firefox">
				<img  src="img/old-browser/ff.png" alt="firefox icon">
			</a>
			
			<a href="http://www.google.com/chrome/" title="Chrome">
				<img  src="img/old-browser/ch.png" alt="Chrome icon">
			</a>
			
			<a href="http://www.opera.com/download/" title="Opera">
				<img  src="img/old-browser/op.png" alt="Opera icon">
			</a>
			
			<a href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie" title="Internet Explorer 8+">
				<img  src="img/old-browser/ie.png" alt="IE icon">
			</a>
		</div>
		
			<a class="close-reveal-modal">&#215;</a>
		</div>
		
		<div id="dyslexia" class="reveal-modal">
			<h1>Open-Dyslexic</h1>
			<p>The font for this website has been changed to Open-Dyslexic. OpenDyslexic is a new open sourced and community driven font created to increase readability for readers with dyslexia.</p>
			<p>read more about Open-Dyslexic or download it <a href="http://dyslexicfonts.com/">here</a></p>
			<a class="close-reveal-modal">&#215;</a>
			
			<p>To change the font back just click on the "Dyslexic?" button in the footer again.</p>
		</div>
		
		<div class="pop-up">
			"Add to homescreen" for best experience.
		</div>

		
		<script src="js/foundation/modernizr.foundation.js" type="text/javascript"></script>
		<script type="text/javascript">

			Modernizr.load([ 
			{
				load: "http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js",
				complete: function () {
					if ( !window.jQuery ) {
						Modernizr.load('js/vendor/jquery-1.8.2.min.js');
						}
					}
				},
			{
				load:[
					"js/vendor/response.min.js",
					"js/jquery.scrollorama.js",
					"js/foundation/jquery.foundation.reveal.js",
					"js/waypoints.min.js",
					"js/main.js"
				]}
			 ]);
		</script>
		
		
	</body>
</html>

<?php

date_default_timezone_set('Europe/London');

$file= fopen("rss/rss.rss", "wb");

//path to directory to scan
$directory = "img/uploads/";

//get all image files with a .jpg extension.
$images = glob($directory . "*.jpg");
usort($images, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));

$now = date("D, d M Y H:i:s T");


$output = "<?xml version=\"1.0\"?>
            <rss version=\"2.0\" xmlns:media='http://search.yahoo.com/mrss/'>
                <channel>
                    <title>Damon grows a beard</title>
                    <link>http://damongrowsabeard.co.uk</link>
                    <description>Damon grows a beard</description>
                    <language>en-us</language>
                    <pubDate>Wed, 24 Oct 2012 08:30:37 MDT</pubDate>
                    <lastBuildDate>$now</lastBuildDate>
                    <docs>http://damongrowsabeard.co.uk</docs>
                    <managingEditor>damon@damongolding.com</managingEditor>
                    <webMaster>damon@damongolding.com</webMaster>";
            
foreach ($images as $image)
{
    $output .= "<item><title>".basename($image)."</title>
                    	<link>http://damongrowsabeard.co.uk#gallery</link>
                    <description>
	                    &lt;p&gt;&lt;img src='http://damongrowsabeard.co.uk/".$image."'/&gt;&lt;/p&gt;
                    </description>
                </item>";
}




$output .= "</channel></rss>";
header("Content-Type: application/rss+xml");


fwrite($file, $output);

fclose($file);


?>

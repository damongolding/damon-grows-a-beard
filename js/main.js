$(document).ready(function() { /* global vars */
	var mobile = Response.band(0, 767);
	var tablet = Response.band(768, 1024);
	var dyslexia;
	var dyslexia_bool = false;
	var pop = $(".pop-up");
	var pop_up;
	var pop_up_bool = false;
	window.location.hash = ""; /* init & local storage */

	function init() {
		dyslexia = localStorage.getItem('dyslexia');
		pop_up = localStorage.getItem('pop_up');
		switch (localStorage.pop_up) {
		case "1":
			pop_up_bool = true;
			break;
		default:
			pop_up_bool = false;
		}
		switch (localStorage.dyslexia) {
		case "1":
			dyslexia_bool = true;
			break;
		default:
			dyslexia_bool = false;
		}
		screensize();
	} /* start scrollorama */
	var scrollorama = $.scrollorama({
		blocks: '.scrollblock',
		enablePin: false
	});
	scrollorama.animate('nav', {
		duration: 95,
		property: 'top',
		end: -95,
		delay: 305
	}); /* response.js for PPI */
	(function(window, document, $, R) { // put the vars you need and match them at the bottom
		Response.create({
			mode: 'src',
			prefix: 'density',
			prop: 'device-pixel-ratio',
			breakpoints: [1, 1.5, 2]
		});
	}(this, this.document, this.jQuery, this.Response)); /* do you suppert backgroud-size?? */
	if ((!Modernizr.backgroundsize) || (!Modernizr.boxshadow) || !(Modernizr.borderradius)) {
		$('#oldbrowser').reveal();
	}
	/*
$(".why").on("click", function() {
		$('#thatswhy').reveal();
	});
*/ 
/* replace beard with img */
	$(".content").find(".six").each(function() {
		//$(this).html( $(this).html().replace(/beard/g,'<img class="replaced" src="img/beard.png" alt="beard" />'));
		$(this).html($(this).html().replace(/beard/g, '<span>beard</span>'));
	});
	$(".day").each(function() {
		$(this).html($(this).html().replace("_", " "));
	}); /* remove .jpg from imge title */
	$(".day").each(function() {
		$(this).text($(this).text().replace('.jpg', ''));
	}); /* add end to ast gallery item */
	$(".gallery").find(".four").last().addClass("end");
	// Smooth scrolling for internal links
	var scrollElement = 'html, body';
	$('html, body').each(function() {
		var initScrollTop = $(this).attr('scrollTop');
		$(this).attr('scrollTop', initScrollTop + 1);
		if ($(this).attr('scrollTop') == initScrollTop + 1) {
			scrollElement = this.nodeName.toLowerCase();
			$(this).attr('scrollTop', initScrollTop);
			return false;
		}
	});
	$("a[href^='#']").click(function(event) {
		event.preventDefault();
		var $this = $(this),
			target = this.hash,
			$target = $(target);
		$(scrollElement).stop().animate({
			'scrollTop': $target.offset().top
		}, 500, 'swing', function() {
			window.location.hash = target;
		});
	}); /* 	switching font */
	$('#font').toggle(function() {
		if (!dyslexia_bool) {
			localStorage.setItem('dyslexia', 1);
			dyslexia_bool = true;
			$("#dyslexia").reveal();
		}
		$("body").css("fontFamily", "opendyslexicregular");
	}, function() {
		$("body").css("fontFamily", "isnt-tilregular");
	}); /* screen width */

	function screensize() {
		if (mobile) {
			if (!pop_up_bool) { /* 	pop up mobile (iphone)*/
				pop.delay(3000).animate({
					opacity: "1",
					bottom: "20px"
				}, "slow", function() {
					setTimeout(function() {
						pop.animate({
							opacity: "0",
							bottom: "10px"
						}, "slow", function() {
							pop.remove();
							localStorage.setItem('pop_up', 1);
						});
					}, 6000)
				});
			} else {
				pop.remove();
			}
		} else if (tablet) {
			if (!pop_up_bool) { /* 	pop up mobile (ipad)*/
				pop.delay(3000).animate({
					opacity: "1",
					top: "20px"
				}, "slow", function() {
					setTimeout(function() {
						pop.animate({
							opacity: "0",
							top: "10px"
						}, "slow", function() {
							pop.remove();
							localStorage.setItem('pop_up', 1);
						});
					}, 6000)
				});
			} else {
				pop.remove();
			}
		} else { /* 	waypoints */
			$(".gallery").waypoint(function(event, direction) {
				if (direction === 'down') {
					$("header").css("display", "none");
				} else {
					$("header").css("display", "block");
				}
			});
		}
	}
	init();
});
# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    Damon Golding	Designer,Deveolper & beard grower 	@DamonLeeGolding

# THANKS

    Movember & Sons		http://uk.movember.com/		for giving me the change and excuse not to shave.

# TECHNOLOGY COLOPHON

    HTML5,
    CSS3,
    jQuery,
    Modernizr,
    Foundation grid system,
    jQuery waypoints,
    Scrollorama,
    LESS,
    LESSphp
    
